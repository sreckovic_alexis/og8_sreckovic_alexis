package modulusbysub;

public class Modulus {
	int i;
	int rest;
	void mod(int a, int b) {
		if(a - b >= 0) {
			i++;
			mod(a - b, b);
		} else {
			rest = a;
			System.out.println("Ganzzahliger Rest: "+rest);
			System.out.println("Ganzzahliger Quotient: "+i);
		}
	}
}
