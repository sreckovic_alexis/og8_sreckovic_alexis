import java.awt.*;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class TestGroupLayout {
	
	

	public static void main(String[] args) {
		// GroupLayout
		// Frame und Container erstellen
		JFrame GroupLayoutFrame = new JFrame("GroupLayoutExample");
		Container contentPanel = GroupLayoutFrame.getContentPane(); 
		
		// GroupLayout erstellen und setzen
		GroupLayout groupLayout = new GroupLayout(contentPanel);  
		contentPanel.setLayout(groupLayout);  
		
		// Komponenten erstellen
		JLabel clickMe = new JLabel("Click Here");  
        JButton button = new JButton("This Button");  
        
        // Komponenten zum GroupLayout hinzufügen
        groupLayout.setHorizontalGroup(  
                groupLayout.createSequentialGroup()  
                            .addComponent(clickMe)  
                            .addGap(10, 20, 100)  
                            .addComponent(button));  
	    groupLayout.setVerticalGroup(  
	                 groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)  
	                            .addComponent(clickMe)  
	                            .addComponent(button));  
	    
	    // Frame packen und sichtbar machen
	    GroupLayoutFrame.pack();  
	    GroupLayoutFrame.setVisible(true); 

	}

}
