import java.awt.*;  
  
import javax.swing.*;  

public class TestCardLayout { 
		    
	public static void main(String[] args) {
		// Frame und CardLayout erstellen
		CardLayout card;  
		JButton b1,b2,b3;  
	    
	    JFrame CardLayoutFrame = new JFrame("CardLayoutExample");
	    Container contentPanel = CardLayoutFrame.getContentPane(); 
	    card = new CardLayout(40,30);  
	    contentPanel.setLayout(card);  
	    
	    // Komponenten erstellen
	    b1=new JButton("Apple");  
	    b2=new JButton("Boy");  
	    b3=new JButton("Cat");  
	    
	    // Komponenten hinzufügen zum CardLayout
	    contentPanel.add("a",b1);
	    contentPanel.add("b",b2);
	    contentPanel.add("c",b3); 
	    
	    // Frame anzeigen
	    CardLayoutFrame.setSize(400,400);  
	    CardLayoutFrame.setVisible(true);  
	    
	}

}
