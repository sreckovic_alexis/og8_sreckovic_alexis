import java.awt.*;  
  
import javax.swing.*;  

public class TestGridBagLayout {

	public static void main(String[] args) {
		// Frame und GridBagLayout erstellen
	    JFrame CardLayoutFrame = new JFrame("CardLayoutExample");
	    Container contentPanel = CardLayoutFrame.getContentPane(); 
	    
	    GridBagLayout GridBagLayoutgrid = new GridBagLayout();  
        GridBagConstraints gbc = new GridBagConstraints();  
        contentPanel.setLayout(GridBagLayoutgrid);  
        CardLayoutFrame.setTitle("GridBag Layout Example");  
        GridBagLayout layout = new GridBagLayout();  
        contentPanel.setLayout(layout);  
        
        // Komponenten mit Abstand im Grid erstellen
        gbc.fill = GridBagConstraints.HORIZONTAL;  
        gbc.gridx = 0;  
        gbc.gridy = 0;  
       
        gbc.gridx = 1;  
        gbc.gridy = 0;  
   
        gbc.fill = GridBagConstraints.HORIZONTAL;  
        gbc.ipady = 20;  
        gbc.gridx = 0;  
        gbc.gridy = 1;  
     
        gbc.gridx = 1;  
        gbc.gridy = 1;  
          
        gbc.gridx = 0;  
        gbc.gridy = 2;  
        gbc.fill = GridBagConstraints.HORIZONTAL;  
        gbc.gridwidth = 2;  
        
        // Komponenten zum Grid hinzufügen
        contentPanel.add(new Button("Button One"), gbc);
        contentPanel.add(new Button("Button Two"), gbc);
        contentPanel.add(new Button("Button Three"), gbc);
        contentPanel.add(new Button("Button Four"), gbc);
        contentPanel.add(new Button("Button Five"), gbc);  
        
        // Frame anzeigen
        CardLayoutFrame.setSize(300, 300);  
        CardLayoutFrame.setVisible(true);  
	}

}
