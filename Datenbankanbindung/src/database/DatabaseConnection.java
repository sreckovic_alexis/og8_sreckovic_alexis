package database;

import java.sql.DriverManager;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

public class DatabaseConnection {
	
	// https://workupload.com/file/AwcMNzXW
	
	private String driver = "com.mysql.jdbc.Driver";
	private String url = "jdbc:mysql://localhost/keks";
	private String user = "root";
	private String password = "";
	
	public boolean insertKekse(String bezeichnung, String sorte) {
		String sql = "INSERT INTO t_kekse (bezeichnung, sorte) VALUES ('"+bezeichnung+"', '"+sorte+"')";
		
		try {
			// JDBC Treiber laden
			Class.forName(driver);
			
			java.sql.Connection con = DriverManager.getConnection(url, user, password);
			
			Statement stmnt = con.createStatement();
			
			stmnt.executeUpdate(sql);
			
			con.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public static void main(String[] args) {
		DatabaseConnection dbc = new DatabaseConnection();
		dbc.insertKekse("gefüllter Keks", "Kokskeks");
	}
}
