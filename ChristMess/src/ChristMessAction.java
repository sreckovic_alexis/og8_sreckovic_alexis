import java.io.IOException;
import java.util.*;

public class ChristMessAction {
	
	public static void main(String[] args) {
		
		//Alle Kinder nach Bravheit auflisten
		//Kinder mit gleichem Bravheitsgrad nach Nachnamen auflisten
		KindGenerator kg = new KindGenerator();
		Kind[] k = new Kind[10000];
		//alle Kindobjekte mit positivem Bravheitsgrad ausgeben, alphabetisch nach Nachnamen sortiert
		System.out.println("Bei Macy's shoppen f�r:");
		try {
			k = kg.generateKinderListe();
//			for (int i = 0; i < k.length; i++) {
//				System.out.println(k[i].getBravheitsgrad());
//			}
			List<Kind> kinder = Arrays.asList(k);
			Collections.sort(kinder);
			for(Kind kind : kinder) {
				if(kind.getBravheitsgrad() > 0)
					System.out.println(kind.toString());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
