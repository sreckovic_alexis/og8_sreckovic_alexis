import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Comparator;

public class KindGenerator {
	
	
	public Kind[] generateKinderListe() throws IOException {

			Kind[] kindarr = new Kind[10000];
			FileReader fr = new FileReader("kinddaten.txt");
			BufferedReader br = new BufferedReader(fr);
			String s;
			int i = 0;
			while((s = br.readLine()) != null) {
				if(i < 10000) {
					String[] parts = s.split(", ");
					kindarr[i] = new Kind();
					kindarr[i].setVorname(parts[0].split(" ")[0]);
					kindarr[i].setNachname(parts[0].split(" ")[1]);
					kindarr[i].setGeburtsdatum(parts[1]);
					kindarr[i].setBravheitsgrad(Integer.parseInt(parts[2]));
					kindarr[i].setOrt(parts[3]);
				}
//				System.out.println(i);
				i++;
			}

			br.close();
			return kindarr;

	}
	//Hier eine Methode erstellen, um die Datei der Kinder auszulesen und mit den Daten eine Liste an Kind-Objekte anlegen

}