import java.util.*;

public class Kind implements Comparable<Object>{
	
	//Attribute
	private String vorname;
	private String nachname;
	private String geburtsdatum;
	private int bravheitsgrad;
	private String ort;
	
	
	public Kind() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public Kind(String vorname, String nachname, String geburtsdatum, int bravheitsgrad, String ort) {
		super();
		this.vorname = vorname;
		this.nachname = nachname;
		this.geburtsdatum = geburtsdatum;
		this.bravheitsgrad = bravheitsgrad;
		this.ort = ort;
	}


	public String getVorname() {
		return vorname;
	}
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	public String getNachname() {
		return nachname;
	}
	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	public String getGeburtsdatum() {
		return geburtsdatum;
	}
	public void setGeburtsdatum(String geburtsdatum) {
		this.geburtsdatum = geburtsdatum;
	}
	public int getBravheitsgrad() {
		return bravheitsgrad;
	}
	public void setBravheitsgrad(int bravheitsgrad) {
		this.bravheitsgrad = bravheitsgrad;
	}
	public String getOrt() {
		return ort;
	}
	public void setOrt(String ort) {
		this.ort = ort;
	}
	
	


	@Override
	public String toString() {
		return "Kind [vorname=" + vorname + ", nachname=" + nachname + ", geburtsdatum=" + geburtsdatum
				+ ", bravheitsgrad=" + bravheitsgrad + ", ort=" + ort + "]";
	}


	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		
		Kind other = (Kind) o;
		
		int i = Integer.compare(this.bravheitsgrad, other.getBravheitsgrad());
	    if (i != 0) return i;

		return this.ort.compareTo(other.getOrt());
	}
	
	
	//Jedes Kind hat einen Nachnamen, Vornamen, Geburtstag, Wohnort und Bravheitsgrad
	//Erstellen Sie einen vollparametrisierten Konstruktor, Getter/Setter und eine toString-Methode

}
