package rechnungsrechner;

public class RechnerFunktionen {
	/* This function adds 10€ as shipping costs to 
	 * the sum if the amount of the products is less than 10  */
	double applyShippingCosts(int amount, double sum) {
		if (amount < 10) {
			sum = sum + 10;
		}
		return sum;
	}
	
	/* This functions applies the tax of 19% to
	 * the passed in sum */
	double applyTax(double sum) {
		double tax = sum * 0.19;
		return sum + tax;
	}
	
}
