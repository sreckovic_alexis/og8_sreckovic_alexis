package rechnungsrechner;

import java.util.Scanner;

public class RechnerTest {

	public static void main(String[] args) {
		RechnerFunktionen rechner = new RechnerFunktionen();
		Scanner scan = new Scanner(System.in);
		System.out.print("Bitte Anzahl des Produkts eingeben: ");
		int amount = scan.nextInt();
		System.out.print("Bitte Einzelpreis des Produkts eingeben: ");
		double price = scan.nextDouble();
		double sum = price * amount;
		System.out.println("Zwischenbetrag: " + sum);
		sum = rechner.applyShippingCosts(amount, sum);
		System.out.println("Rechnungsbetrag mit Versandkosten: " + sum);
		sum = rechner.applyTax(sum);
		System.out.println("Rechnungsbetrag mit Mehrwertsteuer: " + sum);
	}

}
