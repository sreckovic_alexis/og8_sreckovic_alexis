
public class HeapSort {
	public int anzahldurchlaeufe = 0;
	
	public int[] heapsort(int[] a) {
		buildheap(a);
		
		for(int i = a.length -1; i > 0; i--) {
			swap(a, i, 0);
			heapify(a, 0, i);
		}
		
		return a;
	}

	public void buildheap(int[] a) {
		for(int i = (a.length / 2) - 1; i >= 0 ; i--) {
			heapify(a, i, a.length);
		}
	}

	public void heapify(int[] a, int i, int n) {
		while(i <= (n / 2) - 1) {
			anzahldurchlaeufe++;
			System.out.println(anzahldurchlaeufe + "	" + java.util.Arrays.toString(a));
			int kindIndex = ((i+1) * 2) - 1;

			if(kindIndex + 1 <= n -1) {
				if(a[kindIndex] < a[kindIndex+1]) {
					kindIndex++;
				}
			}
			
			if(a[i] < a[kindIndex]) {
				swap(a,i,kindIndex);
				i = kindIndex;
			} else break;
		}
	}

	public void swap(int[] a, int i, int kindIndex) {
		int z = a[i];
		a[i] = a[kindIndex];
		a[kindIndex] = z;
	}
	
	public void resetDurchlaeufe() {
		this.anzahldurchlaeufe = 0;
	}
}
