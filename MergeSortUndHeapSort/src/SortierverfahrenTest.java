
public class SortierverfahrenTest {

	public static void main(String[] args) {
		Sortierverfahren sort = new Sortierverfahren();
		HeapSort hsort = new HeapSort();
		
		int[] input = {20, 30, 10, 4, 3, 9, 5, 44, 22, 11, 9, 5};
		System.out.println("MergeSort");
		System.out.println("-----------");
		System.out.println("Start	" + java.util.Arrays.toString(input));
		sort.setInput(input);
		int[] output = sort.mssort(0, input.length -1);
		sort.resetDurchlaeufe();
		System.out.println("Ende	" + java.util.Arrays.toString(output));
		
		int[] input2 = {20, 30, 10, 4, 3, 9, 5, 44, 22, 11, 9, 5};
		System.out.println("HeapSort");
		System.out.println("-----------");
		System.out.println("Start	" + java.util.Arrays.toString(input2));
		output = hsort.heapsort(input2);
		System.out.println("Ende	" + java.util.Arrays.toString(output));
	}

}
