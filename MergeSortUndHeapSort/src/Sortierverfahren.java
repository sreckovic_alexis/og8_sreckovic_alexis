
public class Sortierverfahren {
	public int[] input;
	public int anzahldurchlaeufe = 0;

	public int[] mssort(int l, int r) {
		if (l < r) {
			int mitte = (l + r) / 2;

			mssort(l, mitte);
			mssort(mitte + 1, r);
			msmerge(l, mitte, r);
		}
		anzahldurchlaeufe++;
		System.out.println(anzahldurchlaeufe + "	" + java.util.Arrays.toString(input));
		return input;
	}

	public void msmerge(int l, int mitte, int r) {
		int[] arr = new int[input.length];
		int i, j;
		for (i = l; i <= mitte; i++) {
			arr[i] = input[i];
		}
		for (j = mitte + 1; j <= r; j++) {
			arr[r + mitte + 1 - j] = input[j];
		}
		i = l;
		j = r;
		for (int k = l; k <= r; k++) {
			if (arr[i] <= arr[j]) {
				input[k] = arr[i];
				i++;
			} else {
				input[k] = arr[j];
				j--;
			}
		}
	}


	public void setInput(int[] input) {
		this.input = input;
	}

	public void resetDurchlaeufe() {
		this.anzahldurchlaeufe = 0;
	}
	
	public int[] getInput() {
		return input;
	}

}
