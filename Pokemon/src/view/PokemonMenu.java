package view;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.time.LocalDate;

import javax.imageio.ImageIO;
import javax.swing.*;
import controller.*;

public class PokemonMenu {
	
	public JLabel createLabel(String text, int font_size) {
        JLabel headline = new JLabel(text);
        headline.setFont(new Font("Serif", Font.PLAIN, font_size));
        return headline;
	}
	
	public JProgressBar createPBar(int size, int maxsize) {
        JProgressBar b = new JProgressBar(); 
        b.setMaximum(maxsize);
        b.setValue(size); 
        b.setStringPainted(true); 
        return b;
	}
	
	PokemonMenu(String name, File bild, int wp, int kp, int maxkp, boolean favorit, String typ,
			double gewicht, double groesse, int bonbons, Attacke[] attacken, LocalDate funddatum) {
		
		JFrame frame = new JFrame("Layout");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        ImageIcon favorit_true = new ImageIcon(this.getClass().getResource("/controller/star_full.png"));
        JLabel label_favorit_true = new JLabel("  ", favorit_true, JLabel.CENTER);
        
        ImageIcon favorit_false = new ImageIcon(this.getClass().getResource("/controller/star_empty.png"));
        JLabel label_favorit_false = new JLabel("  ", favorit_false, JLabel.CENTER);
        
        String bild_path = bild.getPath().replace("\\", "/");
        ImageIcon image = new ImageIcon(this.getClass().getResource(bild_path));
        JLabel label_image = new JLabel("  ", image, JLabel.CENTER);
    
        
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		
        
		JPanel panelwp = new JPanel();
		panelwp.setLayout(new FlowLayout());
        panelwp.add(createLabel("WP", 12));
        panelwp.add(createLabel(String.valueOf(wp), 25));
        if(favorit == true) {
        	panelwp.add(label_favorit_true);
        } else {
        	panelwp.add(label_favorit_false);
        }
		
        JPanel imagepanel = new JPanel();
        imagepanel.setLayout(new BorderLayout());
        imagepanel.setPreferredSize(new Dimension(0, 20));
        imagepanel.add(label_image);
		
		JPanel headlinepanel = new JPanel();
		headlinepanel.setLayout(new FlowLayout());
		headlinepanel.add(createLabel(name, 30));
        
        JPanel pbarpanel = new JPanel(); 
        JProgressBar b = new JProgressBar(); 
        b.setString("KP " + kp + "/" + maxkp);
        b.setMaximum(maxkp);
        b.setValue(kp); 
        b.setStringPainted(true); 
        pbarpanel.add(b);
        
        JPanel detailspanel = new JPanel();
        detailspanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
        detailspanel.add(createLabel("Typ", 12));
        detailspanel.add(createLabel(typ, 25));
        detailspanel.add(createLabel("Gewicht", 12));
        detailspanel.add(createLabel(gewicht+" kg", 25));
        detailspanel.add(createLabel("Groesse", 12));
        detailspanel.add(createLabel(groesse+" m", 25));
        
        JPanel respanel = new JPanel();
        respanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
        respanel.add(createLabel("Sternenstaub", 12));
        respanel.add(createLabel("66726", 25));
        respanel.add(createLabel(name+"-Bonbon", 12));
        respanel.add(createLabel(String.valueOf(bonbons), 25));
        
        int attack_amount = attacken.length;
        JPanel[] attack_panels = new JPanel[attack_amount];
        
        mainPanel.add(panelwp);
        mainPanel.add(imagepanel);
        mainPanel.add(headlinepanel);
        mainPanel.add(pbarpanel);
        mainPanel.add(detailspanel);
        mainPanel.add(respanel);
        
        for (int i = 0; i < attack_amount; i++) {
        	attack_panels[i] = new JPanel();
        	attack_panels[i].setLayout(new FlowLayout(FlowLayout.CENTER, 50, 5));
        	attack_panels[i].add(createLabel(attacken[i].getTyp(), 12));
        	attack_panels[i].add(createLabel(attacken[i].getAttackenname(), 20));
        	attack_panels[i].add(createPBar(attacken[i].getSchaden(), 100));
        	mainPanel.add(attack_panels[i]);
		}
        
        JPanel datepanel = new JPanel();
        datepanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
        datepanel.add(createLabel("Funddatum", 12));
        datepanel.add(createLabel(String.valueOf(funddatum), 20));
        
        JPanel sendpanel = new JPanel();
        sendpanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
        JButton verschicken = new JButton("VERSCHICKEN");
        sendpanel.add(verschicken);
        
        mainPanel.add(datepanel);
        mainPanel.add(sendpanel);
        
        frame.add(mainPanel);
        frame.setPreferredSize(new Dimension(900,800));
        frame.pack();
        frame.setVisible(true);  
		

	}

	


	public static void main(String[] args) {
		
		File bild = new File("/controller/Aerodactyl_Home.png");
		Attacke[] attacken = new Attacke[2];
		attacken[0] = new Attacke("Stahlflügel", 15, "Stahl");
		attacken[1] = new Attacke("Hyperstrahl", 70, "Normal");
		LocalDate ldObj = LocalDate.of(2016, 07, 26);

		Pokemon pokemon = new Pokemon(1, "Aerodactyl", bild, 311, 52, 52, false, "Gestein / Flug", 79.05, 2.03, 26, attacken, ldObj);
		System.out.println(pokemon.toString());
		
		Pokemon pokemon2 = new Pokemon(1, "Aerodactyl", bild, 311, 30, 52, true, "Gestein / Flug", 79.05, 2.03, 26, attacken, ldObj);
		System.out.println(pokemon.toString());
		
		new PokemonMenu(pokemon2.getName(), pokemon2.getBild(), pokemon2.getWp(), pokemon2.getKp(), pokemon2.getMaxkp(), pokemon2.isFavorit(), pokemon2.getTyp(), 
				pokemon2.getGewicht(), pokemon2.getGroesse(), pokemon2.getBonbons(), pokemon2.getAttacken(), pokemon2.getFunddatum());
		
		new PokemonMenu(pokemon.getName(), pokemon.getBild(), pokemon.getWp(), pokemon.getKp(), pokemon.getMaxkp(), pokemon.isFavorit(), pokemon.getTyp(), 
				pokemon.getGewicht(), pokemon.getGroesse(), pokemon.getBonbons(), pokemon.getAttacken(), pokemon.getFunddatum());
		
	}
}
