package controller;

public class Attacke {
	
	// Attribute
	private String attackenname;
	private int schaden;
	private String typ;
	
	// Methoden
	/**
	 * @param attackenname
	 * @param schaden
	 * @param typ
	 */
	public Attacke(String attackenname, int schaden, String typ) {
		super();
		this.attackenname = attackenname;
		this.schaden = schaden;
		this.typ = typ;
	}

	public String getAttackenname() {
		return attackenname;
	}

	public void setAttackenname(String attackenname) {
		this.attackenname = attackenname;
	}

	public int getSchaden() {
		return schaden;
	}

	public void setSchaden(int schaden) {
		this.schaden = schaden;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	@Override
	public String toString() {
		return "Attacke [attackenname=" + attackenname + ", schaden=" + schaden + ", typ=" + typ + "]";
	}
	
	
	
	
}
