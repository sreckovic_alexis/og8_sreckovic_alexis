package controller;

import java.io.File;
import java.time.LocalDate;
import java.util.Arrays;

public class Pokemon {
	
	// Attribute
	private int id;
	private String name;
	private File bild;
	private int wp;
	private int kp;
	private int maxkp;
	private boolean favorit;
	private String typ;
	private double gewicht;
	private double groesse;
	private int bonbons;
	private Attacke[] attacken;
	private LocalDate funddatum;
	
	// Methoden
	public Pokemon() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param id
	 * @param name
	 * @param bild
	 * @param wp
	 * @param kp
	 * @param maxkp
	 * @param favorit
	 * @param typ
	 * @param gewicht
	 * @param groesse
	 * @param sternenstaub
	 * @param bonbons
	 * @param attacken
	 * @param funddatum
	 */
	public Pokemon(int id, String name, File bild, int wp, int kp, int maxkp, boolean favorit, String typ,
			double gewicht, double groesse, int bonbons, Attacke[] attacken, LocalDate funddatum) {
		super();
		this.id = id;
		this.name = name;
		this.bild = bild;
		this.wp = wp;
		this.kp = kp;
		this.maxkp = maxkp;
		this.favorit = favorit;
		this.typ = typ;
		this.gewicht = gewicht;
		this.groesse = groesse;
		this.bonbons = bonbons;
		this.attacken = attacken;
		this.funddatum = funddatum;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the bild
	 */
	public File getBild() {
		return bild;
	}

	/**
	 * @param bild the bild to set
	 */
	public void setBild(File bild) {
		this.bild = bild;
	}

	/**
	 * @return the wp
	 */
	public int getWp() {
		return wp;
	}

	/**
	 * @param wp the wp to set
	 */
	public void setWp(int wp) {
		this.wp = wp;
	}

	/**
	 * @return the kp
	 */
	public int getKp() {
		return kp;
	}

	/**
	 * @param kp the kp to set
	 */
	public void setKp(int kp) {
		this.kp = kp;
	}

	/**
	 * @return the maxkp
	 */
	public int getMaxkp() {
		return maxkp;
	}

	/**
	 * @param maxkp the maxkp to set
	 */
	public void setMaxkp(int maxkp) {
		this.maxkp = maxkp;
	}

	/**
	 * @return the favorit
	 */
	public boolean isFavorit() {
		return favorit;
	}

	/**
	 * @param favorit the favorit to set
	 */
	public void setFavorit(boolean favorit) {
		this.favorit = favorit;
	}

	/**
	 * @return the typ
	 */
	public String getTyp() {
		return typ;
	}

	/**
	 * @param typ the typ to set
	 */
	public void setTyp(String typ) {
		this.typ = typ;
	}

	/**
	 * @return the gewicht
	 */
	public double getGewicht() {
		return gewicht;
	}

	/**
	 * @param gewicht the gewicht to set
	 */
	public void setGewicht(double gewicht) {
		this.gewicht = gewicht;
	}

	/**
	 * @return the groesse
	 */
	public double getGroesse() {
		return groesse;
	}

	/**
	 * @param groesse the groesse to set
	 */
	public void setGroesse(double groesse) {
		this.groesse = groesse;
	}

	/**
	 * @return the bonbons
	 */
	public int getBonbons() {
		return bonbons;
	}

	/**
	 * @param bonbons the bonbons to set
	 */
	public void setBonbons(int bonbons) {
		this.bonbons = bonbons;
	}

	/**
	 * @return the attacken
	 */
	public Attacke[] getAttacken() {
		return attacken;
	}

	/**
	 * @param attacken the attacken to set
	 */
	public void setAttacken(Attacke[] attacken) {
		this.attacken = attacken;
	}

	/**
	 * @return the funddatum
	 */
	public LocalDate getFunddatum() {
		return funddatum;
	}

	/**
	 * @param funddatum the funddatum to set
	 */
	public void setFunddatum(LocalDate funddatum) {
		this.funddatum = funddatum;
	}

	@Override
	public String toString() {
		return "Pokemon [id=" + id + ", name=" + name + ", bild=" + bild + ", wp=" + wp + ", kp=" + kp + ", maxkp="
				+ maxkp + ", favorit=" + favorit + ", typ=" + typ + ", gewicht=" + gewicht + ", groesse=" + groesse
				+ ", bonbons=" + bonbons + ", attacken=" + Arrays.toString(attacken) + ", funddatum=" + funddatum + "]";
	}

	
	
	
}
