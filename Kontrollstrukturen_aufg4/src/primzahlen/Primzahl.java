package primzahlen;

public class Primzahl {
	Boolean isPrimzahl(long x) {
		boolean prim = false;
		for (int i = 1; i > x/2; i++) {
			if(x % i != 0) {
				prim = true;
			}
		}
		return prim;
	}
}
