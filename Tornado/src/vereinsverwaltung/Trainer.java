package vereinsverwaltung;

public class Trainer extends Person {
	private char lizenzklasse;
	private int aufwandsentschädigung;
	
	public Trainer() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Trainer(String name, int telefonnummer, boolean jahr_gezahlt) {
		super(name, telefonnummer, jahr_gezahlt);
		// TODO Auto-generated constructor stub
	}
	
	public char getLizenzklasse() {
		return lizenzklasse;
	}
	public void setLizenzklasse(char lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}
	public int getAufwandsentschädigung() {
		return aufwandsentschädigung;
	}
	public void setAufwandsentschädigung(int aufwandsentschädigung) {
		this.aufwandsentschädigung = aufwandsentschädigung;
	}
}
