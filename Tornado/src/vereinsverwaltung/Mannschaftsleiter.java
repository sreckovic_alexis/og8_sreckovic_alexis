package vereinsverwaltung;

public class Mannschaftsleiter extends Spieler {
	private String name_mannschaft;
	private double rabatt;

	public Mannschaftsleiter() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Mannschaftsleiter(String name, int telefonnummer, boolean jahr_gezahlt) {
		super(name, telefonnummer, jahr_gezahlt);
		// TODO Auto-generated constructor stub
	}

	public String getName_mannschaft() {
		return name_mannschaft;
	}

	public void setName_mannschaft(String name_mannschaft) {
		this.name_mannschaft = name_mannschaft;
	}

	public double getRabatt() {
		return rabatt;
	}

	public void setRabatt(double rabatt) {
		this.rabatt = rabatt;
	}

}
