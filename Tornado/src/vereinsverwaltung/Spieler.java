package vereinsverwaltung;

public class Spieler extends Person {
	private int trikotnummer;
	private String spielerposition;
	
	public Spieler() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Spieler(String name, int telefonnummer, boolean jahr_gezahlt) {
		super(name, telefonnummer, jahr_gezahlt);
		// TODO Auto-generated constructor stub
	}
	
	public int getTrikotnummer() {
		return trikotnummer;
	}

	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}

	public String getSpielerposition() {
		return spielerposition;
	}

	public void setSpielerposition(String spielerposition) {
		this.spielerposition = spielerposition;
	}
	
}
