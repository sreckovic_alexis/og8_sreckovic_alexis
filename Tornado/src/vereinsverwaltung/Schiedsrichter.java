package vereinsverwaltung;

public class Schiedsrichter extends Person {
	private int gepfiffene_spiele;

	public Schiedsrichter() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Schiedsrichter(String name, int telefonnummer, boolean jahr_gezahlt) {
		super(name, telefonnummer, jahr_gezahlt);
		// TODO Auto-generated constructor stub
	}
	
	public int getGepfiffene_spiele() {
		return gepfiffene_spiele;
	}

	public void setGepfiffene_spiele(int gepfiffene_spiele) {
		this.gepfiffene_spiele = gepfiffene_spiele;
	}

	
}
