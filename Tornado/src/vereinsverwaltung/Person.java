package vereinsverwaltung;

public class Person {
	private String name;
	private int telefonnummer;
	private boolean jahr_gezahlt;
	
	public Person() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Person(String name, int telefonnummer, boolean jahr_gezahlt) {
		super();
		this.name = name;
		this.telefonnummer = telefonnummer;
		this.jahr_gezahlt = jahr_gezahlt;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTelefonnummer() {
		return telefonnummer;
	}

	public void setTelefonnummer(int telefonnummer) {
		this.telefonnummer = telefonnummer;
	}

	public boolean isJahr_gezahlt() {
		return jahr_gezahlt;
	}

	public void setJahr_gezahlt(boolean jahr_gezahlt) {
		this.jahr_gezahlt = jahr_gezahlt;
	}
	
	
}
