package de.oszimt.starsim2099;

/**
 * Write a description of class Pilot here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Pilot extends Flugobjekt {

	// Attribute
	private String name;
	private String grad;
	
	
	// Methoden
	public Pilot() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Pilot(String name, String grad, double posX, double posY) {
		super();
		this.name = name;
		this.grad = grad;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGrad() {
		return grad;
	}
	public void setGrad(String grad) {
		this.grad = grad;
	}
	

}
