package de.oszimt.starsim2099;

/**
 * Write a description of class Planet here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Planet extends Flugobjekt {

	// Attribute
	private String name;
	private int hafenanzahl;
	
	// Methoden
	public Planet() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Planet(String name, int hafenanzahl, double posX, double posY) {
		super();
		this.name = name;
		this.hafenanzahl = hafenanzahl;
	}
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getAnzahlHafen() {
		return hafenanzahl;
	}


	public void setAnzahlHafen(int hafenanzahl) {
		this.hafenanzahl = hafenanzahl;
	}
	
	// Darstellung
	public static char[][] getDarstellung() {
		char[][] planetShape = { { '\0', '/', '*', '*', '\\', '\0' }, { '|', '*', '*', '*', '*', '|' },
				{ '\0', '\\', '*', '*', '/', '\0' } };
		return planetShape;

	}
}
