package de.oszimt.starsim2099;

/**
 * Write a description of class Ladung here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Ladung extends Flugobjekt {

	// Attribute
	private String typ;
	private int masse;
	
	// Methoden
	public Ladung() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Ladung(String typ, int masse, double posX, double posY) {
		super();
		this.typ = typ;
		this.masse = masse;
	}
	
	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public int getMasse() {
		return masse;
	}

	public void setMasse(int masse) {
		this.masse = masse;
	}
	
	// Darstellung
	public static char[][] getDarstellung() {
		char[][] ladungShape = { { '/', 'X', '\\' }, { '|', 'X', '|' }, { '\\', 'X', '/' } };
		return ladungShape;
	}
}