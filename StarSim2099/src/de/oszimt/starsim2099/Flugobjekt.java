package de.oszimt.starsim2099;

public class Flugobjekt {
	//Attribute
	private double posX;
	private double posY;
	
	//Methoden
	public Flugobjekt() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Flugobjekt(double posX, double posY) {
		super();
		this.posX = posX;
		this.posY = posY;
	}
	
	public double getPosX() {
		return posX;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosY() {
		return posY;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

	
}
