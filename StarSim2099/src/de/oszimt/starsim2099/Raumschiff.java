package de.oszimt.starsim2099;

/**
 * Write a description of class Raumschiff here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Raumschiff extends Flugobjekt {

	// Attribute
	private String typ;
	private String antrieb;
	private int maxLadekapazitaet;
	private int winkel;
	
	
	// Methoden
	public Raumschiff() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Raumschiff(String typ, String antrieb, int maxLadekapazitaet, int winkel) {
		super();
		this.typ = typ;
		this.antrieb = antrieb;
		this.maxLadekapazitaet = maxLadekapazitaet;
		this.winkel = winkel;
	}
	
	public void setTyp(String typ) {
		this.typ = typ;
	}
	
	public String getTyp() {
		return this.typ;
	}
	
	public void setAntrieb(String antrieb) {
		this.antrieb = antrieb;
	}
	
	public String getAntrieb() {
		return this.antrieb;
	}
	
	public void setMaxLadekapazitaet(int max) {
		this.maxLadekapazitaet = max;
	}
	
	public int getMaxLadekapazitaet() {
		return this.maxLadekapazitaet;
	}
	
	public void setWinkel(int winkel) {
		this.winkel = winkel;
	}
	
	public int getWinkel() {
		return this.winkel;
	}
	
	// Darstellung
	public static char[][] getDarstellung() {
		char[][] raumschiffShape = { 
				{'\0', '\0','_', '\0', '\0'},
				{'\0', '/', 'X', '\\', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'/', '_', '_','_', '\\'},				
		};
		return raumschiffShape;
	}

}
