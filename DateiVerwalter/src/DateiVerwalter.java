import java.io.*;

public class DateiVerwalter {
	
	private File file;
	
	public DateiVerwalter(File file) {
		this.file = file;
	}
	
	public void schreibe(String s) {
		try {
			FileWriter fw = new FileWriter(this.file, true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(s);
			bw.newLine();
			bw.flush();
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("File wahrscheinlich nicht vorhanden");
			e.printStackTrace();
		}
		
	}
	
	public void lesen() throws IOException {
		try {
			FileReader fr = new FileReader(this.file);
			BufferedReader br = new BufferedReader(fr);
			String s;
			while((s = br.readLine()) != null) {
				System.out.println(s);
			}
			br.close();
			//fr.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("File wahrscheinlich nicht vorhanden");
			e.printStackTrace();
		}
		
	}
	
	
	
}
