import java.io.*;

public class PrimzahlRechner{	
	//Methoden
	public PrimzahlRechner() {
		super();
		// TODO Auto-generated constructor stub
	}	
	
	public boolean isPrim(long zahl) {
        boolean Prim = true;
        
        for (long i = 2; i < zahl; i++) {
			if(zahl%i == 0) {
				Prim = false;
			}
		}
        
        if(zahl == 0) {
        	Prim = false;
        }
        
        return Prim;
	}
	
	public void checkEveryUntil(double max) {
		for (int i = 0; i <= max; i++) {
			if(isPrim(i)) {
				File f = new File("primzahlen");
				DateiVerwalter dv = new DateiVerwalter(f);
				dv.schreibe(Integer.toString(i));
			}
		}
	}

	
}
