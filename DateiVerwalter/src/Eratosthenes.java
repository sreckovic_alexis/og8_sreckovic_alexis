import java.io.File;

public class Eratosthenes {
	
	public void getPrimzahlen(int maxCount) {
		int count = 0;
		int maxValue = 1000000000;
        if (maxValue < 2) {
            maxValue = 2;
        }
        boolean[] isPrim = new boolean[maxValue + 1];
        for (int i = maxValue; i >= 2; i--) { // mit true initialisieren
            isPrim[i] = true;
        }
        
        for (int i = 2; count <= maxCount; i++) {
            if (isPrim[i]) {
            	count++;
				File f = new File("primzahlen");
				DateiVerwalter dv = new DateiVerwalter(f);
				dv.schreibe(Integer.toString(i));
                int nextNotPrim = i+i;
                while (nextNotPrim <= maxValue) {
                    isPrim[nextNotPrim] = false;
                    nextNotPrim += i;
                }
            }
        }
        //return isPrim;
    }


}