import java.util.Scanner;

public class SucheTest {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Suche suche = new Suche();

		final long[] daten = new long[20000000];
		// final long gesucht = 1000;
		// final long gesucht = 10000000000L;
		long value = 0;

		for (int i = 0; i < daten.length; i++) {
			value += (Math.random() * ((3000 - 2000) + 1)) + 3000;
			daten[i] = value;
			// System.out.println(daten[i]);
		}

		long worstcase;
		long averagecase;
		long bestcase;

		while (true) {
			System.out.println("0.) linear");
			System.out.println("1.) binär");
			System.out.println("2.) interpoliert");
			int laufzeit = scanner.nextInt();
			switch (laufzeit) {
			case 0:
				worstcase = -1;
				averagecase = daten[(daten.length - 1) / 2];
				bestcase = daten[0];
				System.out.println(suche.lineareSuche(averagecase, daten));
				System.out.println(suche.lineareSuche(bestcase, daten));
				System.out.println(suche.lineareSuche(worstcase, daten));
				break;
			case 1:
				worstcase = -1;
				averagecase = daten[(daten.length - 1) / 2];
				bestcase = daten[0];
				System.out.println(suche.binaereSuche(averagecase, daten));
				System.out.println(suche.binaereSuche(bestcase, daten));
				System.out.println(suche.binaereSuche(worstcase, daten));
				break;
			case 2:
				worstcase = -1;
				averagecase = daten[(daten.length) / 2];
				bestcase = daten[0];
				System.out.println(suche.interpolierteSuche(averagecase, daten));
				System.out.println(suche.interpolierteSuche(bestcase, daten));
				System.out.println(suche.interpolierteSuche(worstcase, daten));
				break;

			default:
				break;
			}

		}
	}

}
