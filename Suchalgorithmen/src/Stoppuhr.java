
public class Stoppuhr {
	//Attribute
	private long startzeit;
	private long endzeit;
	
	//Methoden
	public void start() {
		this.startzeit = System.nanoTime();
	}
	
	public void stopp() {
		this.endzeit = System.nanoTime();
	}
	
	public void reset() {
		this.startzeit = 0;
		this.endzeit = 0;
	}
	
	public long getDauerInMs() {
		return this.endzeit-this.startzeit;
	}
}
