
public class Suche {
	Stoppuhr stoppuhr = new Stoppuhr();

	public int lineareSuche(final long gesucht, final long[] daten) {
		stoppuhr.start();
		for (int i = 0; i < daten.length; i++) {
			if (daten[i] == gesucht) {
				stoppuhr.stopp();
				System.out.println("lineare Laufzeit: " + stoppuhr.getDauerInMs() + " ns");
				return i;
			}
		}
		stoppuhr.stopp();
		System.out.println("lineare Laufzeit: " + stoppuhr.getDauerInMs() + " ns");
		return -1;
	}

	public int binaereSucheRekursiv(final long gesucht, final long[] daten, int start, int ende) {
		int mitte = (start + ende) / 2;
		if (ende < start) {
			return -1;
		}
		if (daten[mitte] == gesucht) {
			return mitte;
		} else if (daten[mitte] < gesucht) {
			return binaereSucheRekursiv(gesucht, daten, mitte + 1, ende);
		} else {
			return binaereSucheRekursiv(gesucht, daten, start, mitte - 1);
		}
	}

	public int binaereSuche(final long gesucht, final long[] daten) {
		stoppuhr.start();
		int result = binaereSucheRekursiv(gesucht, daten, 0, daten.length - 1);
		stoppuhr.stopp();
		System.out.println("binäre Laufzeit: " + stoppuhr.getDauerInMs() + " ns");
		return result;
	}

	public int interpolierteSuche(long schluessel, long daten[]) {
		stoppuhr.start();
		int start = 0;
		int ende = daten.length - 1;
		double versch;
		int x;
		while (schluessel >= daten[start] && schluessel <= daten[ende]) {
			versch = daten[ende] - daten[start];
			x = start + (int) (((double) ende - start) * (schluessel - daten[start]) / versch);

			if (schluessel > daten[x]) {
				start = x + 1;
			} else if (schluessel < daten[x]) {
				ende = x - 1;
			} else {
				stoppuhr.stopp();
				System.out.println("interpolierte Laufzeit: " + stoppuhr.getDauerInMs() + " ns");
				return x;
			}
		}
		stoppuhr.stopp();
		System.out.println("interpolierte Laufzeit: " + stoppuhr.getDauerInMs() + " ns");
		return -1;
	}

}
