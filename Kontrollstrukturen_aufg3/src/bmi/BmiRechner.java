package bmi;

public class BmiRechner {
	/*
	 * Funktion berechnet den BMI
	 */
	double applyBmi(double Körpergewicht, double Körpergröße) {
		double bmi = Körpergewicht / Math.pow(Körpergröße / 100, 2);
		return bmi;
	}
	
	/*
	 * Funktion stuft den BMI ein
	 */
	String applyClassification(double bmi, String gender) {
		// System.out.println(gender+bmi);
		switch (gender) {
		case "w":
			if (bmi < 19) {
				return "Diagnose: Untergewicht";
			}
			else if (bmi >= 19 && bmi <= 24) {
				return "Diagnose: Normalgewicht";
			}
			else if (bmi > 19) {
				return "Diagnose: Übergewicht";
			} else {
				return "Error: 1";
			}
		case "m":
			if (bmi < 20) {
				return "Diagnose: Untergewicht";
			}
			else if (bmi >= 20 && bmi <= 25) {
				return "Diagnose: Normalgewicht";
			}
			else if (bmi > 25) {
				return "Diagnose: Übergewicht";
			}
			else {
				return "Error: 2";
			}


		default:
			return "Error: 3";
		}
	}
}
