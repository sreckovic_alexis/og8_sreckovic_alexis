package bmi;

import java.util.Scanner;


public class BmiRechnerTest {

	public static void main(String[] args) {
		BmiRechner bmi = new BmiRechner();
		Scanner scan = new Scanner(System.in);
		System.out.print("Bitte Körpergewicht eingeben in kg: ");
		double gewicht = scan.nextDouble();
		System.out.print("Bitte Körpergröße eingeben in cm: ");
		double größe =  scan.nextDouble();
		System.out.print("Bitte geben Sie ihr Geschlecht ein (m/w): ");
		String geschlecht =  scan.next();
		double bmi_out = bmi.applyBmi(gewicht, größe);
		System.out.println("BMI: " + bmi_out);
		System.out.println(bmi.applyClassification(bmi_out, geschlecht));
	}


}
