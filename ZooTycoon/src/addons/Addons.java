package addons;

public class Addons {
	private int addonid;
	private String bezeichnung;
	private double verkaufspreis;
	private int bestand;
	private int bestand_erlaubt;
	
	
	public Addons() {
		
	}
	
	public Addons(int addonid, String bezeichnung, double verkaufspreis, int bestand, int bestand_erlaubt) {
		this.addonid = addonid;
		this.bezeichnung = bezeichnung;
		this.verkaufspreis = verkaufspreis;
		this.bestand = bestand;
		this.bestand_erlaubt = bestand_erlaubt;
	}
	
	public int getAddonId() {
		return this.addonid;
	}
	
	public void setAddonId(int addonid) {
		this.addonid = addonid;
	}
	
	public String getBezeichnung() {
		return this.bezeichnung;
	}
	
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	
	public double getVerkaufspreis() {
		return this.verkaufspreis;
	}
	
	public void setVerkaufspreis(double verkaufspreis) {
		this.verkaufspreis = verkaufspreis;
	}
	
	public int getBestand() {
		return this.bestand;
	}
	
	public void setBestand(int bestand) {
		this.bestand = bestand;
	}
	
	public int getBestandErlaubt() {
		return this.bestand_erlaubt;
	}
	
	public void setBestandErlaubt(int bestand_erlaubt) {
		this.bestand_erlaubt = bestand_erlaubt;
	}
	
	public double getGesamtwert(double verkaufspreis, int bestand) {
		double gesamtpreis = verkaufspreis * bestand;
		return gesamtpreis;
	}
	
	public void AddonVerbrauch(int verbrauch) {
		if(this.bestand - verbrauch >= 0) {
			this.bestand = this.bestand - verbrauch;
		}
	}
	
	public void AddonKauf(int kaufmenge) {
		if(this.bestand + kaufmenge <= this.bestand_erlaubt) {
			this.bestand = this.bestand + kaufmenge;
		}
	}
	
	public boolean unlockAchievement(double gesamtpreis, double entsperrwert) {
		if(gesamtpreis >= entsperrwert) {
			return true;
		} else {
			return false;
		}
	}
}
