package addons;

public class AddonsTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int addonid = 1;
		String bezeichnung = "spezielles Tierfutter";
		double verkaufspreis = 0.29;
		int bestand = 1;
		int bestand_erlaubt = 10;
		double entsperrwert = 50;
		Addons addon = new addons.Addons(addonid, bezeichnung, verkaufspreis, bestand, bestand_erlaubt);
		System.out.println("Alte ID: "+addon.getAddonId());
		addon.setAddonId(2);
		System.out.println("Neue ID: "+addon.getAddonId());
		double gesamtwert = addon.getGesamtwert(addon.getVerkaufspreis(), addon.getBestandErlaubt());
		System.out.println(addon.unlockAchievement(gesamtwert, entsperrwert));
	}

}
