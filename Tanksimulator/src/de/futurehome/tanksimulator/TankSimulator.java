package de.futurehome.tanksimulator;
import java.awt.*; 
import javax.swing.*; 
import java.awt.event.*;

@SuppressWarnings("serial")
public class TankSimulator extends Frame {
	
	public Tank myTank;
	
	private Label lblUeberschrift = new Label("Tank-Simulator");
	public  Label lblFuellstand = new Label("     ");
	
	public Button btnBeenden = new Button("Beenden");
	public Button btnEinfuellen = new Button("Einfüllen");
	public Button btnVerbrauchen = new Button("Verbrauchen");
	public Button btnZuruecksetzen = new Button("Zurücksetzen");
	
	private Panel pnlNorth = new Panel();
	private Panel pnlCenter = new Panel(new FlowLayout());
	private Panel pnlSouth = new Panel(new GridLayout(1, 0));
	
	
	static JProgressBar b; 
	
	static JSlider meinSlider;
	
	private MyActionListener myActionListener = new MyActionListener(this);

	public TankSimulator() {
		super("Tank-Simulator");
		
		myTank = new Tank(0);
		
		b = new JProgressBar();
		
		b.setValue((int)myTank.getFuellstand()); 
		  
	    b.setStringPainted(true); 
		
	    // JSlider-Objekt wird erzeugt
	    meinSlider = new JSlider();
	  
	    // Mindestwert wird gesetzt
	    meinSlider.setMinimum(0);
	    // Maximalwert wird gesetzt
	    meinSlider.setMaximum(4);
	  
	    // Die Abstände zwischen den 
	    // Teilmarkierungen werden festgelegt
	    meinSlider.setMajorTickSpacing(1);
	    meinSlider.setMinorTickSpacing(1);
	  
	    // Standardmarkierungen werden erzeugt 
	    meinSlider.createStandardLabels(1);
	  
	    // Zeichnen der Markierungen wird aktiviert
	    meinSlider.setPaintTicks(true);
	  
	    // Zeichnen der Labels wird aktiviert
	    meinSlider.setPaintLabels(true);
	    
		this.lblUeberschrift.setFont(new Font("", Font.BOLD, 16));
		this.pnlNorth.add(this.lblUeberschrift);
  
		
		this.pnlCenter.add(b);
		this.pnlCenter.add(meinSlider);
		this.pnlCenter.add(this.lblFuellstand);
		this.pnlSouth.add(this.btnEinfuellen);
		this.pnlSouth.add(this.btnVerbrauchen);
		this.pnlSouth.add(this.btnZuruecksetzen);
		this.pnlSouth.add(this.btnBeenden);
		this.add(this.pnlNorth, BorderLayout.NORTH);
		this.add(this.pnlCenter, BorderLayout.CENTER);
		this.add(this.pnlSouth, BorderLayout.SOUTH);
		this.pack();
		this.setVisible(true);
		
		// Ereignissteuerung
		this.btnEinfuellen.addActionListener(myActionListener);
		this.btnVerbrauchen.addActionListener(myActionListener);
		this.btnZuruecksetzen.addActionListener(myActionListener);
		this.btnBeenden.addActionListener(myActionListener);
	}

	public static void main(String argv[]) {
		TankSimulator f = new TankSimulator();
	}
}