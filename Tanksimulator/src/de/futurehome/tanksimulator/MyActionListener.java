package de.futurehome.tanksimulator;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.sql.Timestamp;


public class MyActionListener implements ActionListener {
	public TankSimulator f;

	public MyActionListener(TankSimulator f) {
		this.f = f;
	}
	
	public Timestamp getTimestamp() {
		Date date= new Date();

		long time = date.getTime();

		Timestamp ts = new Timestamp(time);
		return ts;
	}

	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == f.btnBeenden)
			System.exit(0);
		
		if (obj == f.btnEinfuellen) {
			 double fuellstand = f.myTank.getFuellstand();
			 if(fuellstand <= 95) {
				 fuellstand = fuellstand + 5;
				 System.out.println(getTimestamp() + " Tank um 5% aufgefüllt");
			 }
			 f.myTank.setFuellstand(fuellstand);
			 f.b.setValue((int)fuellstand);
			 f.lblFuellstand.setText("Füllstand in %: "+fuellstand);
		}
		
		if (obj == f.btnVerbrauchen) {
			 double fuellstand = f.myTank.getFuellstand();
			 if(fuellstand >= 2) {
				 fuellstand = fuellstand - f.meinSlider.getValue();
				 System.out.println(getTimestamp() + " Tank um " + f.meinSlider.getValue() + "% verbraucht");
			 }
			 f.myTank.setFuellstand(fuellstand);
			 f.b.setValue((int)fuellstand);
			 f.lblFuellstand.setText("Füllstand in %: "+fuellstand);
		}
		
		if (obj == f.btnZuruecksetzen) {
			 double fuellstand = f.myTank.getFuellstand();
			 fuellstand = 0;
			 System.out.println(getTimestamp() + " Tank entleert");
			 f.myTank.setFuellstand(fuellstand);
			 f.b.setValue((int)fuellstand);
			 f.lblFuellstand.setText("Füllstand in %: "+fuellstand);
		}

	}
}