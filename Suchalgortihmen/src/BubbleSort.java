
public class BubbleSort {

	public int[] blasenSortieren(int[] array) {
		int count = 0;
		for(int n = array.length; n > 1; n--) {
			for(int i = 0; i < n-1; i++) {
				count++;
				if(array[i] > array[i+1]) {
					int temp = array[i+1];
					array[i+1] = array[i];
					array[i] = temp;
				}
			}
			System.out.println(java.util.Arrays.toString(array));
		}
		System.out.println("Anzahl der Vergleiche: "+count);
		return array;
	}
}
