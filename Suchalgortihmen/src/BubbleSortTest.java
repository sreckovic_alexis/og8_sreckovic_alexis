public class BubbleSortTest {

	public static void main(String[] args) {
		BubbleSort bubble = new BubbleSort();
		int[] array = {6, 4, 100, 3, 2, 1, 88, 55, 22, 3};
		int[] sortiert = bubble.blasenSortieren(array);
		System.out.println(java.util.Arrays.toString(sortiert));

	}

}
