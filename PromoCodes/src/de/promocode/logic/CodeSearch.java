package de.promocode.logic;

public class CodeSearch {

	public static int NOT_FOUND = -1;

	/**
	 * Sucht einen Code in der Liste.
	 * 
	 * @param list
	 *            Liste der Codes im Long-Format
	 * @param searchValue
	 *            Gesuchter Code
	 * @return Index des Codes, wenn er vorhanden ist, NOT_FOUND wenn er nicht
	 *         gefunden werden konnte
	 */
	public static int findPromoCode(long[] list, long searchValue) {
		return interpolierteSuche(searchValue, list);
	}
	
	public static int interpolierteSuche(long schluessel, long daten[]) {
		int start = 0;
		int ende = daten.length - 1;
		double versch;
		int x;
		while (schluessel >= daten[start] && schluessel <= daten[ende]) {
			versch = daten[ende] - daten[start];
			x = start + (int) (((double) ende - start) * (schluessel - daten[start]) / versch);

			if (schluessel > daten[x]) {
				start = x + 1;
			} else if (schluessel < daten[x]) {
				ende = x - 1;
			} else {
				return x;
			}
		}
		return -1;
	}

}
