
public class Primzahlen {
	
	//Attribute
	Stoppuhr timer = new Stoppuhr();
	
	//Methoden
	public Primzahlen() {
		super();
		// TODO Auto-generated constructor stub
	}	
	
	public boolean isPrim(long zahl) {
		timer.start();
        boolean Prim = true;
        
        for (long i = 2; i < zahl; i++) {
			if(zahl%i == 0) {
				Prim = false;
			}
		}
        
        if(zahl == 0) {
        	Prim = false;
        }
        
        timer.stopp();
        System.out.println("lineare Suche: "+ timer.getDauerInMs() + " ms");
        return Prim;
	}
	
	public void checkEveryUntil(double max) {
		for (int i = 0; i <= max; i++) {
			if(isPrim(i)) {
				System.out.println(i + " ist eine Primzahl.");
			} else {
				System.out.println(i + " ist keine Primzahl.");
			}
		}
	}

	
}
