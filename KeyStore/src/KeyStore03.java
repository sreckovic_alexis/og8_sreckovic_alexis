import java.util.*; 
public class KeyStore03 {
	private HashSet<String> h = new HashSet<String>(); 
	
	public boolean add(String eintrag) {
		h.add(eintrag);
		return true;
	}
	
	public int indexOf(String input) {
		if(!h.contains(input)) {
			return -1;
		}
		
		int k = 0;
        Iterator<String> i = h.iterator(); 
        while (i.hasNext()) {
        	if(input.equals(i.next())) {
        		k++;
        	}
        }
        return k;
	}
	
	public void remove(int index) {
		int k = 0;
        Iterator<String> i = h.iterator(); 
        while (i.hasNext()) {
        	if(k == index) {
        		h.remove(i.next());
        		return;
        	}
        	k++;
        }
        
	}
	
	public String printArr() {
        return h.toString();
	}
}
