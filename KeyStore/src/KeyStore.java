
public class KeyStore {
	private String[] arr = new String[1];
	private int arr_len = 1;
	
	public boolean add(String eintrag) {
		if(arr_len == 1) {
			this.arr[0] = eintrag;
			arr_len++;
		} else {
			//System.out.println(2);
			String[] temp_arr = new String[this.arr.length+1];
			//System.out.println(temp_arr.length);
			for (int i = 0; i < this.arr.length; i++) {
				temp_arr[i] = this.arr[i];
			}
			temp_arr[temp_arr.length-1] = eintrag;
			this.arr = temp_arr;
			arr_len++;
		}
		return true;
	}
	
	public int indexOf(String input) {
		for (int i = 0; i < this.arr.length; i++) {
			if(input.equals(this.arr[i])) {
				return i;
			}
		}
		return -1;
	}
	
	public void remove(int index) {
		this.arr_len--;
		this.arr[index] = null;
		String[] temp = new String[this.arr_len];
		for (int i = 0, j=0; i < this.arr.length; i++) {
			if(this.arr[i] != null) {
				temp[j] = this.arr[i];
				j++;
			}
		}
		this.arr = temp;
	}
	
	public String printArr() {
		return java.util.Arrays.toString(this.arr);
	}
}
