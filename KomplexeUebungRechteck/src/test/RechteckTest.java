package test;

import model.Rechteck;
import controller.BunteRechteckeController;
import view.Zeichenflaeche;

public class RechteckTest {
	
	public static void main(String[] args) {

		Rechteck rechteckParameterlos0 = new Rechteck();
		Rechteck rechteckParameterlos1 = new Rechteck();
		Rechteck rechteckParameterlos2 = new Rechteck();
		Rechteck rechteckParameterlos3 = new Rechteck();
		Rechteck rechteckParameterlos4 = new Rechteck();
		
		
		Rechteck rechteck0 = new Rechteck(10,10,30,40);
		Rechteck rechteck1 = new Rechteck(25,25,100,20);
		Rechteck rechteck2 = new Rechteck(260,10,200,100);
		Rechteck rechteck3 = new Rechteck(5,500,300,25);
		Rechteck rechteck4 = new Rechteck(100,100,100,100);
		Rechteck rechteck5 = new Rechteck(200,200,200,200);
		Rechteck rechteck6 = new Rechteck(800,400,20,20);
		Rechteck rechteck7 = new Rechteck(800,450,20,20);
		Rechteck rechteck8 = new Rechteck(850,400,20,20);
		Rechteck rechteck9 = new Rechteck(855,455,25,25); 
		
		// String rechteck0String = rechteck0.toString();
		// System.out.println(rechteck0String.equals("Rechteck [x=10, y=10, breite=30, hoehe=40]"));
		
		BunteRechteckeController rcontroller = new BunteRechteckeController();
		rcontroller.add(rechteck0);
		rcontroller.add(rechteck1);
		rcontroller.add(rechteck2);
		rcontroller.add(rechteck3);
		rcontroller.add(rechteck4);
		rcontroller.add(rechteck5);
		rcontroller.add(rechteck6);
		rcontroller.add(rechteck7);
		rcontroller.add(rechteck8);
		rcontroller.add(rechteck9);
		
		// System.out.println(rcontroller.toString());
		
	    Rechteck eck10 = new Rechteck(-4,-5,-50,-200);
	    System.out.println(eck10); //Rechteck [x=-4, y=-5, breite=50, hoehe=200]
	    Rechteck eck11 = new Rechteck();
	    eck11.setX(-10);
	    eck11.setY(-10);
	    eck11.setBreite(-200);
	    eck11.setHoehe(-100);
	    Rechteck eck12 = new Rechteck(-9,-10,-10,-80);
	    System.out.println(eck11);//Rechteck [x=-10, y=-10, breite=200, hoehe=100]
	    System.out.println(eck11.enthaelt(190, 90));
	    System.out.println(eck11.enthaelt(-10, -10));
	    System.out.println(eck11.enthaelt(eck12));
	    System.out.println(eck11.generiereZufallsRechteck());
	}

}
