package model;

public class Rechteck {
	
	Punkt q = new Punkt();
	
	//Attribute
	private int breite;
	private int hoehe;
	
	//Methoden
	
	public void setX(int x) {
		//this.x = x;
		q.setX(x);
	}
	
	public void setY(int y) {
		//this.y = y;
		q.setY(y);
	}
	
	public void setBreite(int breite) {
		this.breite = Math.abs(breite);
	}
	
	public void setHoehe(int hoehe) {
		this.hoehe = Math.abs(hoehe);
	}
	
	public Rechteck() {
		super();
		setX(0);
		setY(0);
		setBreite(0);
		setHoehe(0);
	}
	
	
	public Rechteck(int x, int y, int breite, int hoehe) {
		super();
		setX(x);
		setY(y);
		setBreite(breite);
		setHoehe(hoehe);
	}

	@Override
	public String toString() {
		return "Rechteck [x=" + getX() + ", y=" + getY() + ", breite=" + breite + ", hoehe=" + hoehe + "]";
	}


	public int getBreite() {
		return breite;
	}

	public int getHoehe() {
		return hoehe;
	}
	
	public int getX() {
		return q.getX();
	}

	public int getY() {
		return q.getY();
	}
	
	public boolean enthaelt(Punkt p) {
		int x = p.getX();
		int y = p.getY();
		int minX = q.getX();
		int maxX = q.getX() + getBreite();
		int minY = q.getY();
		int maxY = q.getY() + getHoehe();
		if(x >= minX && x <= maxX && y >= minY && y <= maxY) {
			return true;
		}
		return false;
	}
	
	public boolean enthaelt(int x, int y) {
		Punkt p = new Punkt(x, y);
		if(enthaelt(p)) {
			return true;
		}
		return false;
	}
	
	public boolean enthaelt(Rechteck rechteck) {
		int x = rechteck.getX();
		int y = rechteck.getY();
		int breite = rechteck.getBreite();
		int hoehe = rechteck.getHoehe();
		
		for (int i = x; i <= x + breite; i++) {
			if(!enthaelt(i, y)) {
				return false;
			}
		}
		
		for(int i = y; i <= y + hoehe; i++) {
			if(!enthaelt(x, i)) {
				return false;
			}
		}
		
		return true;
	}
	
	public static Rechteck generiereZufallsRechteck() {
		int breite = (int)(1200.0 * Math.random());
		int hoehe = (int)(1000.0 * Math.random());
		int x = (int)(1200.0 * Math.random());
		int y = (int)(1000.0 * Math.random());
		if(x + breite > 1200 || y + hoehe > 1000) {
			return generiereZufallsRechteck();
		}
		Rechteck rndm = new Rechteck(x, y, breite, hoehe);
		return rndm;
	}

	

}
