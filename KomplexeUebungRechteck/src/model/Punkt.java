package model;

public class Punkt {
	// Attribute
	private int x;
	private int y;
	
	// Methoden
	public Punkt() {
		super();
		this.x = 0;
		this.y = 0;
	}

	public Punkt(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}
	
	public boolean equals(Punkt p) {
		
		if (this.x == p.getX() && this.y == p.getY()) {
	    return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "Punkt [x=" + x + ", y=" + y + "]";
	}

	
	
	
	
}
