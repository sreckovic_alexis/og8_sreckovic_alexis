package controller;
import java.util.Iterator;
import java.util.LinkedList;

import model.Rechteck;

public class BunteRechteckeController {
	
	//Attribute
	private LinkedList<Rechteck> rechtecke;
	
	
	
    public BunteRechteckeController() {
		super();
		this.rechtecke = new LinkedList<Rechteck>();
	}

	public void add(Rechteck rechteck) {
    	this.rechtecke.add(rechteck);
    }
    
    public void reset() {
    	this.rechtecke.clear();
    }
    
    
	/**
	 * @return the rechtecke
	 */
	public LinkedList<Rechteck> getRechtecke() {
		return rechtecke;
	}
	
	
	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}
	
	public void generiereZufallsRechtecke(int anzahl) {
		this.reset();
		for (int i = 0; i < anzahl + 1; i++) {
			this.add(model.Rechteck.generiereZufallsRechteck());
		}
	}
	

	public static void main(String[] args) {
		BunteRechteckeController brc = new BunteRechteckeController();
		brc.generiereZufallsRechtecke(25);
		System.out.println(brc.toString());
	}


	
}
