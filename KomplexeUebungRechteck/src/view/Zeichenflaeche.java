package view;

import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;
import java.util.ListIterator;

import javax.swing.JPanel;


import controller.BunteRechteckeController;
import model.Rechteck;

public class Zeichenflaeche extends JPanel {
	
	//Attribute
	BunteRechteckeController rcontroller = new BunteRechteckeController();
	
	public Zeichenflaeche(BunteRechteckeController rcontroller) {
		super();
		this.rcontroller = rcontroller;
	}


	@Override
	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		for (Rechteck r : rcontroller.getRechtecke()) {
			g.drawRect(r.getX(), r.getY(), r.getBreite(), r.getHoehe());	
		}
 
	}
	
	
}
