package git_taschenrechner;

import java.util.Scanner;

public class TaschenrechnerTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner myScanner = new Scanner(System.in);
		Taschenrechner ts = new Taschenrechner();

		int swValue;

		// Display menu graphics
		System.out.println("============================");
		System.out.println("|   MENU SELECTION DEMO    |");
		System.out.println("============================");
		System.out.println("| Options:                 |");
		System.out.println("|        1. Addieren       |");
		System.out.println("|        2. Subtrahieren   |");
		System.out.println("|        3. Multiplizieren |");
		System.out.println("|        4. Dividieren     |");
		System.out.println("|        5. Exit           |");
		System.out.println("============================");
		System.out.print(" Select option: ");
		swValue = myScanner.next().charAt(0);

		// Switch construct
		switch (swValue) {
		case '1':
			// Addieren
			System.out.print("First summand: ");
			int a = myScanner.nextInt();
			System.out.print("Second summand: ");
			int b = myScanner.nextInt();
			System.out.println(a + " + " + b + " = " + ts.add(a, b));
			break;
		case '2':
			// Subtrahieren
			System.out.print("Minuend: ");
			int c = myScanner.nextInt();
			System.out.print("Subtrahend: ");
			int d = myScanner.nextInt();
			System.out.println(c + " - " + d + " = " + ts.sub(c, d));
			break;
		case '3':
			// Multiplizieren
			System.out.print("First Factor: ");
			int e = myScanner.nextInt();
			System.out.print("Second Factor: ");
			int f = myScanner.nextInt();
			System.out.println(e + " * " + f + " = " + ts.mul(e, f));
			break;
		case '4':
			// Dividieren
			System.out.print("Dividend: ");
			int g = myScanner.nextInt();
			System.out.print("Divisor: ");
			int h = myScanner.nextInt();
			System.out.println(g + " / " + h + " = " + ts.div(g, h));
			break;
		case '5':
			// Exit
			System.exit(1);
			break;

			
		default:
			System.out.println("Invalid selection");
			break; // This break is not really necessary
		}

	}

}