
public class Rechner {
	/* Funktion berechnet Grenzwert anhand eingegebener Parameter */
	void grenzwert(double n, double startwert, double summand) {
		double grenzwert = n * 2;
		
		while(n > startwert) {
			startwert = startwert + summand;
			System.out.println(startwert);
		}
		
	}
}
